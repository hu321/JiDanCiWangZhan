# 这个文件夹包含对数据库的操作封装的API

### 数据库说明
* 数据库名English
* 集合名word
* 文档结构 
	* word:StringField
	* meaning:StringField
         

### 文件说明

* mongo.py<br>
```

```
* * *
* words.txt<br>
	单词文件，暂时的格式是：单词+tab+tab+意思<br>
	如 ‘China\t\t中国’
* * *
* insert.py<br>
	将单词文件导入到数据库
### 使用
* 将单词写入words.txt中
* ```python insert.py```<br>
	这样就将单词输入数据库中了



