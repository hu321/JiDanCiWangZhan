import json
from Mongo import Mongo
from mongoengine import *
from os import remove
with open("words.txt",'r') as f:
    with open('result.json','w+') as j:
        j.write("[\n")
        for i in f:
            if i and i!="\n":
                j.write('{\n')
                print i.split("\t\t")
                j.write('"word":'+'"%s",\n'%i.strip('\n').split("\t\t")[0])
                j.write('"meaning":'+'"%s"\n'%i.strip('\n').split("\t\t")[1])
                j.write("},\n")
        j.write('{}\n')
        j.write("]\n")
        
with open("result.json","r+") as f:
     text=''.join(f.readlines())
     obj=json.loads(text)
     obj.pop()
     print obj
     f.seek(0)
     f.write(json.dumps(obj))

collection_word={
                "word":StringField(),
                "meaning":StringField()
                }
a=Mongo("English","word",**collection_word)
with open('result.json','r') as f:
    text = ''.join(f.readlines())
    js = json.loads(text)
    for i in js:
        a.insert(**i)
    print a.findall()
remove("result.json")
